# Graphin

GRAPHIN.LIB is my library for Borland-C++ 3.x helping to develop graphical interfaces for DOS
and it was built on top of old version of SVGA256.OBJ (BGI driver for SVGA 256-color graphics):

    SVGA 256 Colour BGI Device Driver (SVGA256) 2.31 - Dec 06, 1991
    Copyright (c) 1991 Jordan Hargraphix

Latest source codes of SVGA256 under MIT-license could be found here: https://github.com/jharg93/SvgaBGI

## Walsh Explorer

One of the examples of GRAPHIN usage is my application "Walsh Explorer" that could be found in WALSHEXP folder.
I built this app for my Diploma Thesis that I defended in February 1996 for my Master of Computer Science degree
back in Russia. Then I didn't have enough time to integrate Huffman coding into the program that already provided
working Walsh-Hadamard based compression (along with couple other algorithms), eventhough Huffman coder/decoder code
was mostly ready. Now I found the time and finally finished the program by adding a few different compression options
to explore (and also removed some unneeded stuff as fractal compression that was part of original program):

![](walshexp_ape4_x2.png "WALSHEXP.EXE")

With all this I'm introducing a new graphics format that I called WHI (Walsh+Huffman Image compression)
that is beating JPEG by quality for comparable compression ratio (I think).
Also this format should be simple enough to be implemented on retro platforms with limited memory
and computational resources (initially I developed it on 486 machine as 16-bit software).

## WHI file format

WHI is a file format to represent images compressed by applying methods listed below:
- Fast 2D Walsh-Hadamard transform over grayscale image
- Quantization of samples to lower number of bits (with storing a few outliers separately)
- Run-length encoding (RLE) for zero samples using a few traversal strategies (diagonal, horizontal, vertical etc.)
- Removing trailing zeros completely to reduce size of remaining data (fully implemented only in 2023)
- And finally optional Huffman coding to reduce size of serialized data even further (this part was integrated in 2023)

## BWS file format

This is a simple file format created in 1995 to represent grayscale images with 6 bits per pixel:

2 bytes - width in little endian mode

2 bytes - height in little endian mode

then sequence of bits 6 bits per pixel (from left to right, from top to bottom) to represent grayscale levels 0..63

Size of grayscale image 64x64 is always 3076 bytes and we are calculating compression ratio based on this number

## Examples

EXAMPLES folder has a number of examples that use GRAPHIN library as this test:

![](graftst1_001.png "GRAFTST1.EXE")

or some RGB experimentation on 256-color VGA screen:

![](rgb_view_female_x2.png "RGB_VIEW.EXE")

it was a result of running RGB_VIEW FEMALE inside of WALSHEXP/BWS_FOTO directory - it reads 3 grayscale files
FEMALER.BWS, FEMALEG.BWS, FEMALEB.BWS (left 3 images) and combines them as color channels to a single RGB image
zoomed in 3 times with dithering.

## Legal notes

Both algoritms (Walsh and Huffman) are considered antique for decades and I believe nobody can say now that
this particular program or file format may potentially infringe some copyrights or patents that still active.

However, this app is using a number of grayscale resources for testing. I collected those resources mostly in 90s
from different places and if you think some specific ones are copyrighted and can not be used for research purposes
please let me know. For now I'm removing LENA, ELAINE and TIFFANY because those images are not considered appropriate
anymore as per https://sipi.usc.edu/database/database.php?volume=misc

## License

I'm releasing some classes as PUBLIC DOMAIN - namelly BWS, BITIO and UNIHUFF.

All examples and tools (in EXAMPLES and TOOLS folders respectively) are PUBLIC DOMAIN too.

But the rest of the code (WALSHEXP and GRAPHIN itself) is covered by MIT license:

    Copyright (c) 1995,1996,2023 A.A.Shabarshin <me@shaos.net>
    
    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom
    the Software is furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
