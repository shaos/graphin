// GRAPHIN.H - GRAPHical INterface for DOS <header>
// Created by A.A.Shabarshin on 30-Nov-1995
// Last update: 22-Nov-2023

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <dos.h>
#include <dir.h>
#include <graphics.h>

#ifndef __GRAPHIN_H
#define __GRAPHIN_H
#define __SVGA_H

#define GRAPHIN_BUILD_H 0x20231122L
#define BUTTON_OFF  0
#define BUTTON_DOWN 1
#define BUTTON_UP   2
#define SYMB_X   8
#define SYMB_Y   8
#define OFFS_X   4
#define OFFS_Y   2
#define OFF      0
#define ON       1

#define SVGA256_320x200 0
#define SVGA256_640x400 1
#define SVGA256_640x480 2
#define SVGA256_800x600 3
#define SVGA256_1024x768 4

#ifndef _RGB_STRUCT
#define _RGB_STRUCT
struct RGB
{ signed char r,g,b;
  RGB(int r1,int g1,int b1){r=r1;g=g1;b=b1;};
  RGB(){r=63;g=63;b=63;};
  RGB& Clamp()
  { if(r<0)r=0;if(r>63)r=63;
    if(g<0)g=0;if(g>63)g=63;
    if(b<0)b=0;if(b>63)b=63;
    return *this;
  }
};
#endif

typedef unsigned char DacPalette256[256][3];

extern int VIDEO;
extern int GRAPHIN_SOUND;
extern int GRAPHIN_PALETTE;
extern FILE* GRAPHIN_AUTOMATE;
extern long GRAPHIN_BUILD_LIB;
extern int MENU_COLOR_ACTIVE;
extern int BUTTON_COLOR_UP;
extern int BUTTON_COLOR_RIGHT;
extern int BUTTON_COLOR_BACKGROUND;
extern int BUTTON_COLOR_LEFT;
extern int BUTTON_COLOR_DOWN;
extern int BUTTON_COLOR_TEXT;

#define COLOR_BLACK        0
#define COLOR_BLUE         1
#define COLOR_GREEN        2
#define COLOR_CYAN         3
#define COLOR_RED          4
#define COLOR_MAGENTA      5
#define COLOR_BROWN        6
#define COLOR_LIGHTGRAY    7
#define COLOR_DARKGRAY     8
#define COLOR_LIGHTBLUE    9
#define COLOR_LIGHTGREEN   10
#define COLOR_LIGHTCYAN    11
#define COLOR_LIGHTRED     12
#define COLOR_LIGHTMAGENTA 28  // close enough
#define COLOR_YELLOW       103 // close enough
#define COLOR_WHITE        255 // brightest gray

#define COLOR_GRAY(x) 192+((x>63)?63:x)

int OpenGraph256(int k);
int CloseGraph(void);
int Automate(char *fname);
void SetColorEGA(int i);
void SetColorRGB(RGB c,int *r=NULL,int *g=NULL,int *b=NULL);
void SetColorGRAY(int g);
#define Arc arc
#define Bar bar
#define Bar3D bar3d
#define Circle circle
#define ClearDevice cleardevice
#define ClearViewPort clearviewport
#define DrawPoly drawpoly
#define Ellipse ellipse
#define FillEllipse fillellipse
#define FillPoly fillpoly
#define FloodFill floodfill
#define GetArcCoords getarccoords
#define GetAspectRatio getaspectratio
#define GetBkColor getbkcolor
#define GetColor getcolor
#define GetFillPattern getfillpattern
#define GetFillSettings getfillsettings
#define GetImage getimage
#define GetLineSettings getlinesettings
#define GetMaxColor getmaxcolor
#define GetMaxMode getmaxmode
#define GetMaxX getmaxx
#define GetMaxY getmaxy
#define GetPixel getpixel
#define GetTextSettings gettextsettings
#define GetViewSettings getviewsettings
#define GetX getx
#define GetY gety
#define ImageSize imagesize
#define Line line
#define LineRel linerel
#define LineTo lineto
#define MoveRel moverel
#define MoveTo moveto
#define OutText outtext
#define OutTextXY outtextxy
#define PieSlice pieslice
#define PutImage putimage
#define PutPixel putpixel
#define Rectangle rectangle
#define RestroreCrtMode restorecrtmode
#define Sector sector
#define SetActivePage setactivepage
#define SetAspectRatio setaspectratio
#define SetBkColor setbkcolor
#define SetColor setcolor
#define SetFillPattern setfillpattern
#define SetFillStyle setfillstyle
#define SetLineStyle setlinestyle
#define SetTextJustify settextjustify
#define SetTextStyle settextstyle
#define SetUserCharsize setusercharsize
#define SetViewPoint setviewport
#define SetVisualPage setvisualpage
#define SetWriteMode setwritemode
#define TextHeight textheight
#define TextWidth textwidth

struct Palette256
{    int stnd,ok;
     DacPalette256 d;
     Palette256(){InitG();};
     Palette256(int m){InitG();ok=SetStandardPalette(m);};
     int SetStandardPalette(int);
     int GetStandardPalette(void){return stnd;};
     ~Palette256(){};
     void SetPalette(int i,RGB col);
     void SetAllPalette(void);
     RGB GetPalette(int i);
     void GetAllPalette(void);
     void InitG(void);
};

class Button
{int  x0,y0,    // Top-left coordinates
      col,row,  // Button dimensions
      status,   // Status ( BUTTON_UP, BUTTON_OFF, BUTTON_DOWN )
      fshow,    // Visibility flag
      c_up,     // Color of top side
      c_right,  // Color of right side
      c_button, // Color of the button itself
      c_left,   // Color of left side
      c_down,   // Color of down side
      c_symbol; // Color of symbols
 char *str;     // Pointer to string
 char **image;  // Copy of the screen covered by button
 int  InitG(void){return OpenGraph256(GRAPHIN_PALETTE);};
 void Set(void);
 void SaveImage(void);
 void LoadImage(void);
 void DeleteImage(void);
public:
 Button(void){Set();};
 Button(int x,int y,int c,int r,int s=BUTTON_UP);
 Button(int x,int y,char *st,int s=BUTTON_UP);
~Button(){if(str!=NULL) delete str;DeleteImage();};
 void SetStatus(int s);
 void SetColors(int cu,int cr,int ck,int cl,int cd);
 void SetColorSymbol(int c){c_symbol=c;if(fshow)Show();};
 void SetColorButton(int c){c_button=c;if(fshow)Show();};
 int GetColorSymbol(void){return c_symbol;};
 int GetColorButton(void){return c_button;};
 void ShiftColors(int d);
 int GetCol(void) {return col;};
 int GetRow(void) {return row;};
 int SetSize(int c,int r=0);
 int SetString(char *st);
 char* GetString(void) {return str;};
 int GetStatus(void) {return status;};
 int GetX(void) {return x0;};
 int GetY(void) {return y0;};
 int SetXY(int x,int y);
 int Draw(void);
 int Show(void);
 int Hide(void);
 int Press(void);
};

class Menu
{int col,row,   // Menu dimensions
     activ,     // Identifier of active item
     num,maxn;  // Number of menu items and max possible number of them
 Button *k;     // Pointer to item buttons
 int *o;        // Return on action
public:
 Menu(int m=10);             // Constuctor with number of items
~Menu(){delete k;delete o;}; // Destructor
 int Append(int,char*);      // Add menu item
 int Activate(int x,int y,int m=-1); // Activate menu
 int Disable(int); // Make menu item inactive (used for menu headers)
 int Enable(int);  // Enable menu item (if it was disabled before)
};

struct StrE   // Structure of string element for lists
{ char *stri; // Ponter to string
  StrE *next; // Pointer to the next element
  StrE *prev; // Pointer to the previous element
  StrE(char *s){stri=new char[strlen(s)+1];strcpy(stri,s);};
 ~StrE(){delete stri;};
};

class Panel
{int col,row,  // Panel dimensions
     activ,    // Active element of the list
     num,      // Number of elements in the list
     nst;      // Number of string in the window
 StrE *sf;     // Pointer to the first element of the list
 StrE *sl;     // Pointer to the last element of the list
public:
 Panel(int str); // Constructor
~Panel();        // Destructor
 int Append(char* s,int n=-1); // Add string to the list
 int Delete(int); // Delete string from the list by index
 int Find(char *); // Find string and return its index
 char* Activate(int x,int y,int n=-1); // Activate panel
};

class FilePanel : public Panel // Class of File Panel
{
public:
 FilePanel(char *sh,int s); // Constructor with wildcard
};

#endif
